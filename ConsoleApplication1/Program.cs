﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeometryClassLibrary;
using VisualGeometryDebugger;

namespace ConsoleApplication1
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Point point = PointGenerator.MakePointWithInches(5, 5);

            VisualGeometryDebugger.PointVisualizer.TestShowVisualizer(point);

            Console.Read();
        }
    }
}
