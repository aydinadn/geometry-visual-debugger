﻿using Microsoft.VisualStudio.DebuggerVisualizers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeometryClassLibrary;
using System.Windows.Forms;

[assembly: System.Diagnostics.DebuggerVisualizer(
typeof(VisualGeometryDebugger.LineSegmentVisualizer),
typeof(VisualizerObjectSource),
Target = typeof(GeometryClassLibrary.LineSegment),
Description = "LineSegment Visualizer")]

namespace VisualGeometryDebugger
{

    /// <summary>
    /// A Visualizer for Point.  
    /// 
    /// <example>
    /// Add the following to Point's definition to see this visualizer when debugging instances of Point:
    /// [DebuggerVisualizer(typeof(LineSegmentVisualizer))]
    /// [Serializable]
    /// public class SomeType
    /// {
    ///  ...
    /// }
    /// </example>
    /// 
    /// </summary>
    public class LineSegmentVisualizer  : DialogDebuggerVisualizer
    {
        protected override void Show(IDialogVisualizerService windowService, IVisualizerObjectProvider objectProvider)
        {
            if (windowService == null)
            {
                throw new ArgumentNullException("windowService");
            }
            if (objectProvider == null)
            {
                throw new ArgumentNullException("objectProvider");
            }

            // TODO: Get the object to display a visualizer for.
            //       Cast the result of objectProvider.GetObject() 
            //       to the type of the object being visualized.
            LineSegment objectToDraw = objectProvider.GetObject() as LineSegment;


            if (objectToDraw != null)
            {

                Visualizer3D drawingWindow = new Visualizer3D();
                //drawingWindow.AddGeometryLibraryPointToCanvas(objectToDraw);
                drawingWindow.BuildTestSolid();
                drawingWindow.Show();
                System.Windows.Threading.Dispatcher.Run();
            }

            
        }

        /// <summary>
        /// Tests the visualizer by hosting it outside of the debugger.
        /// </summary>
        /// <param name="objectToVisualize">The object to display in the visualizer.</param>
        public static void TestShowVisualizer(object objectToVisualize)
        {
            VisualizerDevelopmentHost visualizerHost = new VisualizerDevelopmentHost(objectToVisualize, typeof(PointVisualizer));
            visualizerHost.ShowVisualizer();
        }
    }
}
