﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VisualGeometryDebugger
{
    /// <summary>
    /// Interaction logic for GeometryVisualizer.xaml
    /// </summary>
    public partial class Visualizer2D : Window
    {

        public void AddGeometryLibraryPointToCanvas(GeometryClassLibrary.Point passedPoint)
        {

            Console.Out.WriteLine(passedPoint.X.Inches + ", " + passedPoint.Y.Inches);

            // Create a red Ellipse.
            Ellipse myEllipse = new Ellipse();

            // Create a SolidColorBrush with a red color to fill the  
            // Ellipse with.
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();

            // Describes the brush's color using RGB values.  
            // Each value has a range of 0-255.
            mySolidColorBrush.Color = Color.FromArgb(255, 255, 255, 0);
            myEllipse.Fill = mySolidColorBrush;
            myEllipse.StrokeThickness = 2;
            myEllipse.Stroke = Brushes.Black;

            // Set the width and height of the Ellipse.
            myEllipse.Width = 10;
            myEllipse.Height = 10;

            myEllipse.Margin = new Thickness(passedPoint.X.Millimeters, passedPoint.Y.Millimeters, passedPoint.X.Millimeters, passedPoint.Y.Millimeters);

            GeometryCanvas.Children.Add(myEllipse);

        }

        public Visualizer2D()
        {
            InitializeComponent();
        }
    }
}
